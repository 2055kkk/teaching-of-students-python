"""7.	Дана следующая функция y=f(x):
y = 2x - 10, если x > 0
y = 0, если x = 0
y = 2 * |x| - 1, если x < 0
Требуется найти значение функции по переданному x."""

x = int(input('x = '))


def func_1(x_f):
    if x_f > 0:
        y = 2 * x_f - 10
    elif x_f == 0:
        y = 0
    else:
        y = 2 * abs(x_f) - 1
    return y


print(func_1(x))

